const status = response => {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response);
    }
    return Promise.reject(new Error(response.statusText));
};
const obtenerJson = response => { return response.json(); }

exports.status = status;
exports.obtenerJson = obtenerJson;