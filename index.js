const timestamp = require('time-stamp');
const readline = require('readline');
const data = require('./Data-API');
const files = require('./Files-API');
const fs = require('fs');
var globalIndicator = '';
var globalFecha;

var lector = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

var leerOpcion = (lectura) => {
    return new Promise(function(resolve, reject) {
        console.log('MENU: ')
        console.log('1. Actualizar indicadores');
        console.log('2. Promediar');
        console.log('3. Mostrar Valor mas actual');
        console.log('4. Mostrar mínimo histórico');
        console.log('5. Mostrar máximo histórico');
        console.log('6. Salir');
        lectura.question('Elija una opcion: ', opcion => {
            resolve(opcion);
        });
    });

}
leerOpcion(lector).then((opcion) => {
    menu(parseInt(opcion));
}).catch(console.error);

function menu(opcion) {
    switch (opcion) {
        case 1:
            console.log('Opcion 1');
            fetch('https://mindicador.cl/api').then(data.status).then(data.obtenerJson).then(ignore).then((info) => {
                var tiempoactual = timestamp('DDMMYY-HHmmss');
                fs.writeFile("datos/" + tiempoactual + '.ind', JSON.stringify(info), (err) => {
                    (console.err)
                });
            }).then(() => { leerOpcion(lector).then((opcion) => { menu(parseInt(opcion)) }) });
            break;
        case 2:
            console.log('Promediar');
            selectIndicator(lector).then(files.accessFile).then(files.promedio).then(console.log).then(() => { leerOpcion(lector).then((opcion) => { menu(parseInt(opcion)) }) });
            break;
        case 3:
            console.log('Mostrar Valor mas actual');
            files.lastFile().then(console.log).then(() => { leerOpcion(lector).then((opcion) => { menu(parseInt(opcion)) }) });
            break;
        case 4:
            console.log('Mostrar mínimo histórico');
            files.minimoHistorico().then(files.minimoGlobal).then(console.log).then(() => { leerOpcion(lector).then((opcion) => { menu(parseInt(opcion)) }) });
            break;
        case 5:
            console.log('Opcion 5');
            files.maximoHistorico().then(files.maximoGlobal).then(console.log).then(() => { leerOpcion(lector).then((opcion) => { menu(parseInt(opcion)) }) });

            break;
        case 6:
            lector.close();
            process.exit(0);
            break;
        default:
            console.log('Ingrese una opción valida!');
            leerOpcion(lector).then((opcion) => {
                menu(parseInt(opcion))
            });
            break;
    }
}

const fetch = require('node-fetch');


const ignore = (datos) => {
    var fechaGuardado = timestamp('DD-MM-YY');
    var horaGuardado = timestamp('HH:mm:ss');
    //var datos = JSON.parse(json);
    var info = '{"fechaGuardado":' + JSON.stringify(fechaGuardado) + ',"horaGuardado":' + JSON.stringify(horaGuardado) + ',';
    for (var element in datos) {
        if (element === 'dolar' || element === 'euro') {
            info += JSON.stringify(element) + ":" + JSON.stringify(datos[element]) + ",";
        }
        if (element === 'tasa_desempleo') {
            info += JSON.stringify(element) + ":" + JSON.stringify(datos[element]);
        }
    }
    info += '}';
    console.log(info);
    if (info.length > 0) {
        return Promise.resolve(JSON.parse(info));
    } else {
        return Promise.reject(new Error(response.Error));
    }

}


const selectIndicator = (lect) => {
    return new Promise(function(resolve, reject) {
        console.log('Inidcadores: ')
        console.log('1. Dolar');
        console.log('2. Euro');
        console.log('3. Tasa de desempleo');
        lect.question('Elija un indicador: ', indicator => {
            resolve(indicator);
        });
    });
}