var minvalorD = 9999;
var minvalorE = 9999;
var minvalorT = 9999;
var maxvalorD = 0;
var maxvalorE = 0;
var maxvalorT = 0;
var fechaD = 0;
var fechaE = 0;
var fechaT = 0;
var globalFecha = [];
const fs = require('fs');
const accessFile = (indicator) => {
    return new Promise(function(resolve, reject) {
        var path = '../Evaluacion/datos';
        globalIndicator = indicator;
        while (globalFecha.length) {
            globalFecha.pop();
        }
        var valor = [];
        fs.readdir(path, (err, files) => {
            files.forEach((file) => {
                getFile(path + '/' + file).then(JSON.parse).then(fechas).then((insertFecha) => {
                    globalFecha.push(insertFecha);
                });
                valor.push(getFile(path + '/' + file).then(JSON.parse).then(elementValue));
            });
            resolve(Promise.all(valor));
        });
    });

}
const getFile = fileName => {
    return new Promise((resolve, reject) => {
        // El trabajo se hace dentro de la promesa
        fs.readFile(fileName, (err, data) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(data);
        });
    });
}
const fechas = (datos) => {
    return new Promise(function(resolve, reject) {
        //console.log(datos['fechaGuardado'] + ',' + datos['horaGuardado'])
        //+ ',' + datos['horaGuardado']
        resolve(datos['fechaGuardado']);
    });

}
const elementValue = (datos) => {
    var valor = 0;
    return new Promise(function(resolve, reject) {
        if (globalIndicator == '') {
            reject(new Error(response.Error))
        } else {
            valor = datos[globalIndicator].valor;
            resolve(valor);

        }
    });

}
const promedio = (valores) => {
    var suma = 0;
    var prom = 0;

    globalFecha.sort(comparar);
    var primeraFecha = globalFecha[0];
    var ultimaFecha = globalFecha[globalFecha.length - 1];
    var div = valores.length;

    for (let i = 0; i < valores.length; i++) {
        suma += valores[i];
    }
    prom = suma / div;

    return Promise.resolve('El promedio es: ' + prom + '\n' + 'Rango de fechas: ' + primeraFecha + ' al ' + ultimaFecha);

}

function comparar(a, b) {
    var fecha1 = a.split('-');
    var fecha2 = b.split('-');

    if (fecha1[2] > fecha2[2]) {
        return 1;
    } else if (fecha1[2] == fecha2[2] && fecha1[1] > fecha2[1]) {
        return 1;
    } else if (fecha1[2] == fecha2[2] && fecha1[1] == fecha2[1] && fecha1[0] > fecha2[0]) {
        return 1;
    }

    if (fecha1[2] < fecha2[2]) {
        return -1;
    } else if (fecha1[2] == fecha2[2] && fecha1[1] < fecha2[1]) {
        return -1;
    } else if (fecha1[2] == fecha2[2] && fecha1[1] == fecha2[1] && fecha1[0] < fecha2[0]) {
        return -1;
    }

    return 0;
}
const lastFile = () => {
    return new Promise(function(resolve, reject) {
        var path = '../Evaluacion/datos';
        var valor = [];
        var nombre;
        fs.readdir(path, (err, files) => {
            resolve(getFile(path + '/' + files[files.length - 1]).then(JSON.parse).then(valorActual));

        });
    });
}
const valorActual = (datos) => {
    var fecha = datos['fechaGuardado'];
    var hora = datos['horaGuardado'];
    var valorD;
    var valorE;
    var valorT;
    var info = '';
    valorD = datos['dolar'].valor;
    valorE = datos['euro'].valor;
    valorT = datos['tasa_desempleo'].valor;
    info += 'Fecha guardado: ' + fecha + '\n' + 'Hora guardado: ' + hora + '\n' + 'Valor del Dolar: ' + valorD + '\n' + 'Valor del Euro: ' + valorE + '\n' + 'Valor de la tasa de desempleo: ' + valorT;
    return Promise.resolve(info);
}
const minimoHistorico = () => {
    return new Promise(function(resolve, reject) {
        var path = '../Evaluacion/datos';
        while (globalFecha.length) {
            globalFecha.pop();
        }
        var valor = [];
        fs.readdir(path, (err, files) => {
            files.forEach((file) => {
                getFile(path + '/' + file).then(JSON.parse).then(fechas).then((insertFecha) => {
                    globalFecha.push(insertFecha);
                });
                valor.push(getFile(path + '/' + file).then(JSON.parse).then(minimo));
            });
            resolve(Promise.all(valor));
        });
    });
}
const minimo = (datos) => {
    return new Promise(function(resolve, reject) {
        //info = '';
        if (datos['dolar'].valor < minvalorD) {
            minvalorD = datos['dolar'].valor;
            fechaD = datos['fechaGuardado'];
        }
        if (datos['euro'].valor < minvalorE) {
            minvalorE = datos['euro'].valor;
            fechaE = datos['fechaGuardado'];
        }
        if (datos['tasa_desempleo'].valor < minvalorT) {
            minvalorT = datos['tasa_desempleo'].valor;
            fechaT = datos['fechaGuardado'];
        }
        //info += 'Rango de fechas: ' + primeraFecha + ' ' + ultimaFecha + '\n' + 'valor minimo del dolar: ' + valorD + '\n' + 'Fecha: ' + fechaD;
        resolve();
    });

}
const minimoGlobal = () => {
    var info = '';
    globalFecha.sort(comparar);
    var primeraFecha = globalFecha[0];
    var ultimaFecha = globalFecha[globalFecha.length - 1];

    info += 'Rango de fechas: ' + primeraFecha + ' al ' + ultimaFecha + '\n' +
        'valor minimo del dolar: ' + minvalorD + ' ' + 'Fecha: ' + fechaD + '\n' +
        'valor minimo del euro: ' + minvalorE + ' ' + 'Fecha: ' + fechaE + '\n' +
        'valor minimo de la tasa desempleo: ' + minvalorT + ' ' + 'Fecha: ' + fechaT;
    return Promise.resolve(info);
}
const maximoHistorico = () => {
    return new Promise(function(resolve, reject) {
        var path = '../Evaluacion/datos';
        while (globalFecha.length) {
            globalFecha.pop();
        }
        var valor = [];
        fs.readdir(path, (err, files) => {
            files.forEach((file) => {
                getFile(path + '/' + file).then(JSON.parse).then(fechas).then((insertFecha) => {
                    globalFecha.push(insertFecha);
                });
                valor.push(getFile(path + '/' + file).then(JSON.parse).then(maximo));
            });
            resolve(Promise.all(valor));
        });
    });
}
const maximo = (datos) => {
    return new Promise(function(resolve, reject) {
        //info = '';
        if (datos['dolar'].valor > maxvalorD) {
            maxvalorD = datos['dolar'].valor;
            fechaD = datos['fechaGuardado'];
        }
        if (datos['euro'].valor > maxvalorE) {
            maxvalorE = datos['euro'].valor;
            fechaE = datos['fechaGuardado'];
        }
        if (datos['tasa_desempleo'].valor > maxvalorT) {
            maxvalorT = datos['tasa_desempleo'].valor;
            fechaT = datos['fechaGuardado'];
        }
        //info += 'Rango de fechas: ' + primeraFecha + ' ' + ultimaFecha + '\n' + 'valor minimo del dolar: ' + valorD + '\n' + 'Fecha: ' + fechaD;
        resolve();
    });

}
const maximoGlobal = () => {
    var info = '';
    globalFecha.sort(comparar);
    var primeraFecha = globalFecha[0];
    var ultimaFecha = globalFecha[globalFecha.length - 1];

    info += 'Rango de fechas: ' + primeraFecha + ' al ' + ultimaFecha + '\n' +
        'valor máximo del dolar: ' + maxvalorD + ' ' + 'Fecha: ' + fechaD + '\n' +
        'valor máximo del euro: ' + maxvalorE + ' ' + 'Fecha: ' + fechaE + '\n' +
        'valor máximo de la tasa desempleo: ' + maxvalorT + ' ' + 'Fecha: ' + fechaT;
    return Promise.resolve(info);
}
exports.accessFile = accessFile;
exports.promedio = promedio;
exports.lastFile = lastFile;
exports.minimoHistorico = minimoHistorico;
exports.minimoGlobal = minimoGlobal;
exports.maximoHistorico = maximoHistorico;
exports.maximoGlobal = maximoGlobal;